'use strict';
const tab = document.querySelector('.main__tab');
tab.addEventListener('click', (event) => {
    if (event.target.className === 'tabs-title') {
        document.querySelectorAll('.tabs-title.active,.main__content-service.active').forEach(item => {
            item.classList.remove('active');
        })
        event.target.classList.add('active');
        const id = event.target.getAttribute('data-id');
        document.querySelector(id).classList.add('active');
    }
})

const filter = document.querySelector('.main__our-work__menu');
const loadMore = document.querySelector('.main__btn__load-more');
const box = document.querySelectorAll('.main__our-work__content__img');
filter.addEventListener('click', (event) => {
    if (event.target.className === "list") {
        document.querySelectorAll('.list.active').forEach(item => {
            item.classList.remove('active');
        })
        event.target.classList.add('active')
    }
    const target = event.target.getAttribute('data-filter')
    document.querySelectorAll('.show').forEach(item => {
        if (!item.classList.contains(target) && target !== 'all') {
            box.forEach(item => {
                if (!item.classList.contains(target)) {
                    item.style.display = "none";
                }
            })
        } else {
            item.style.display = "block";
        }
    })
})
let currentItem = 12;
loadMore.addEventListener('click', (e) => {
    for (let i = currentItem; i < currentItem + 12; i++) {
        box[i].classList.add('show');
    }
    currentItem += 12;
    if (currentItem >= box.length) {
        loadMore.style.display = 'none';
    }
})

//<---------------------------- JQ------------------------->

$(document).ready(function () {
    $('.big-slider').slick({
        arrows: false,
        fade: true,
        asNavFor: '.small-slider',
        waitForAnimate: false,
    })
    $('.small-slider').slick({
        slidesToShow: 4,
        sped: 100,
        centerMode: true,
        centerPadding: '5px',
        focusOnSelect: true,
        asNavFor: '.big-slider',

    });

})
window.onload = function () {
    const elem = document.querySelector('.grid');
    const ms = new Masonry(elem, {
        columnWidth: 363,
        itemSelector: '.grid__items',
        gutter: 30,
    })
}












